package ca.notyare.itembuilder;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class ItemBuilder extends JavaPlugin {

    private ItemStack item;
    public ItemBuilder() {
        item = new ItemStack(Material.AIR);
    }
    public ItemBuilder(Material type) {
        item = new ItemStack(type);
    }
    public ItemBuilder setMaterial(Material material) {
        item.setType(material);
        return this;
    }
    public ItemBuilder setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }
    public ItemBuilder setDisplayName(String displayName) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        item.setItemMeta(meta);
        return this;
    }
    public ItemBuilder setLore(List<String> lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        item.setItemMeta(meta);
        return this;
    }
    public ItemBuilder addItemFlag(ItemFlag flag) {
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(flag);
        item.setItemMeta(meta);
        return this;
    }
    public ItemBuilder addEnchantment(Enchantment enchantment, int level, boolean ignoreLevelRestriction) {
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(enchantment, level, ignoreLevelRestriction);
        item.setItemMeta(meta);
        return this;
    }
    public ItemStack getItem() {
        return item;
    }
}
